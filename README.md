# Front-End Coding Dojo

This monorepo contains a set of structured lessons, summarising essential topics for Computer Engineering. 

## Recommended Reading:
* Cracking the Coding Interview
* Introduction to Algorithms

## End Goal: To confidently code and to understand the basics needed for Computer Engineering.

## Main Aim’s:

* To understand the following Data Structures; runtime and spacetime complexities, characteristics and how code them 
    - Singular/Doubly Linked Lists
    - Trees/Tries and Graphs
    - Stacks and Queues
    - Heaps
    - Vector’s / ArrayLists
    - Hash Tables

* To understand the following Algorithms;  runtime and spacetime complexities, characteristics and how code them:
    - Breadth-first Search
    - Depth-first Search
    - Binary Search
    - Merge Sort
    - Quick Sort

* Programming Concepts
    - Recursion
    - Reflection
    - Big O

* JavaScript memory management
    - Memory HEAP
    - Call Stack
        * Side Effects:
          - To increase exposure to modern JavaScript - Iterator’s, Generator Functions, getters, setters etc…

* To gain a deep understanding of React:
    - Rendering Components and Elements/JSX representation
    - Usage of Context/Higher Order Functions/Functional patterns
    - The rendering Lifecycle and the VirtualDOM
